import React from 'react';
import './App.css';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import LoginView from "./view/LoginView/LoginView";
import NewTestView from "./view/NewTestView/NewTestView";
import ModalProvider from "./component/Modal/Modal";
import { TemplateProvider } from "./container/context/template/Template";

function App() {
  return (
    <BrowserRouter>
      <ModalProvider>
        <div className="app-container">
          <Switch>
            <Route path="/login">
              <LoginView/>
            </Route>
            <Route path="/">
              <TemplateProvider>
                <NewTestView/>
              </TemplateProvider>
            </Route>
          </Switch>
        </div>
      </ModalProvider>
    </BrowserRouter>
  );
}

export default App;
