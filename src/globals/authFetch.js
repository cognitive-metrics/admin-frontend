const authFetch = (input, init) => {
  console.log({...init});
  return fetch(input, {
    ...init,
    headers: {
      ...init.headers,
      "Authorization": `Basic ${localStorage.getItem("login")}`
    }
  });
};

const debounce = (time) => (debounceFn) => {
  let timeoutID;
  return () => {
    !!timeoutID && clearTimeout(timeoutID);
    timeoutID = setTimeout(debounceFn, time);
  };
};

export { debounce };
export default authFetch;
