import React, { useContext, useEffect, useState } from "react";
import authFetch from "../../globals/authFetch";
import SaveTemplateModal from "../../component/SaveTemplateModal/SaveTemplateModal";
import { ModalContext } from "../../component/Modal/Modal";
import { GAME_KINDS } from "../context/template/Template";

const defaultTemplateChoice = {
  isChosen: false,
  index: 0
};

const useTemplate = () => {

  const [templates, setTemplates] = useState([
    {...GAME_KINDS.DUAL_N_BACK.default},
    {...GAME_KINDS.DUAL_N_BACK.default, nameOfTemplate: "Standard 2", noOfTests: 0},
    {...GAME_KINDS.DUAL_N_BACK.default, nameOfTemplate: "Standard 3", noOfTests: 1},
    {...GAME_KINDS.DUAL_N_BACK.default, nameOfTemplate: "Standard 4", noOfTests: 2},
    {...GAME_KINDS.DUAL_N_BACK.default, nameOfTemplate: "Standard 5", noOfTests: 3}
  ]);
  const [templateChoice, setTemplateChoice] = useState(defaultTemplateChoice);
  const [testKind, setTestKind] = useState(GAME_KINDS.DUAL_N_BACK);
  const {setModalContent} = useContext(ModalContext);

  useEffect(() => {
    authFetch(testKind.templatesUrl, {})
      .then(res => res.ok ? res.json() : undefined)
      .then(data => !!data && setTemplates(data))
      .then(() => setTemplateChoice(defaultTemplateChoice))
  }, [testKind, setTemplateChoice]);

  const saveTemplate = (current) => () => {
    console.log("Current template: ", current);
    const saveFn = (name) => authFetch(testKind.templatesUrl, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({...current, nameOfTemplate: name})
    }).then(res => res.ok ? res.json() : undefined)
      .then(template => {
        if (!!template) {
          setTemplates([
            template,
            ...templates
          ]);
          setModalContent(undefined);
        }
        // setTemplateChoice({isChosen: true, index: 0});
      });

    setModalContent(<SaveTemplateModal save={saveFn}/>)
  };

  const chooseTemplate = (index) => setTemplateChoice({
    ...templateChoice,
    index: index,
    isChosen: true
  });

  const deleteTemplate = (name) => {
    authFetch(testKind.templatesUrl + "/" + name, {
      method: "DELETE"
    }).then(res => res.ok ? res.json() : undefined)
      .then(data => !!data && setTemplates(data))
  };

  return {
    templateChoice,
    templates,
    saveTemplate,
    setTemplateChoice,
    setTestKind,
    testKind,
    chooseTemplate,
    deleteTemplate
  };
};

export default useTemplate;
