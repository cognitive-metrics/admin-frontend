import React, { createContext } from "react";
import useTemplate from "../../hook/useTemplate";

const GAME_KINDS = {
  DUAL_N_BACK: {
    default: {
      nameOfTemplate: "Standard",
      noOfTests: 20,
      noOfVerbalHits: 5,
      noOfSpatialHits: 5,
      timePerTest: 2000,
      level: 2,
      subjectId: '',
      noOfRounds: 1
    },
    templatesUrl: "http://localhost:8080/api/dual-n-back/template",
    createTestUrl: "http://localhost:8080/api/dual-n-back"
  },
  SHAPE_SHIFTER: {
    default: {
      nameOfTemplate: "Standard",
      noOfTests: 20,
      minNoOfTransitions: 3,
      maxNoOfTransitions: 5,
      timePerTest: 3000,
      velocity: 1,
      subjectId: '',
      noOfRounds: 1
    },
    templatesUrl: "http://localhost:8080/api/shapeshifter/template",
    createTestUrl: "http://localhost:8080/api/shapeshifter"
  },
  COLLISIONS: {
    default: {
      minEdgeDistance: 20,
      minTimeBetweenCollisions: 2000,
      maxTimeBetweenCollisions: 10000,
      minTimeVisibleBeforeCollision: 5000,
      pointRadius: 10,
      minPointDistance: 20,
      maxTimeVisibleBeforeCollision: 10000,
      maxGameElapsed: 180000,
      noOfDummies: 100,
      disappearAfter: 2000,
      rerenderRate: 20,
      seed: "abcd",
      subjectId: ""
    },
    templatesUrl: "http://localhost:8080/api/collisions/template",
    createTestUrl: "http://localhost:8080/api/collisions"
  }
};

const TemplateContext = createContext({});

const TemplateProvider = ({children}) => {

  const templateHook = useTemplate();

  return (
    <TemplateContext.Provider value={templateHook}>
      {children}
    </TemplateContext.Provider>
  )
};

export { GAME_KINDS, TemplateContext, TemplateProvider };
