import React, {useContext, useEffect, useState} from "react";
import NewDualNBack from "../../component/NewDualNBack/NewDualNBack";
import NewShapeShifter from "../../component/NewShapeShifter/NewShapeShifter";
import authFetch from "../../globals/authFetch";
import {GAME_KINDS, TemplateContext} from "../context/template/Template";
import "../../component/NewTestPanel/game.css";
import NewCollisionDetection from "../../component/NewCollisionDetection/NewCollisionDetection";

const Game = ({testKind, setUrl}) => {
    const [current, setCurrent] = useState({...testKind.default});
    const {templateChoice, templates, saveTemplate, setTemplateChoice} = useContext(TemplateContext);

    useEffect(() => {
        if (templateChoice.isChosen && templateChoice.index !== undefined) {
            setCurrent({...templates[templateChoice.index]})
        }
    }, [templateChoice, templates]);

    useEffect(() => {
        setCurrent(testKind.default);
    }, [testKind]);

    useEffect(() => {
        console.log("Current: ", current);
        console.log("Test kind default: ", testKind.default);
    }, [current]);

    const changeCurrent = (newProperty) => {
        setCurrent({...current, ...newProperty, nameOfTemplate: ""});
        setTemplateChoice({...templateChoice, isChosen: false, index: undefined});
    };

    const createTest = () => {
        authFetch(testKind.createTestUrl, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(current)
        }).then(res => res.ok && res.text())
            .then(data => {
                console.log(data);
                setUrl(data)
            })
            .catch(console.error)
    };

    useEffect(() => {
        console.log("GAME");
        console.log("State changed: ", {current, templateChoice, templates});
    }, [templates, current, templateChoice]);

    return (
        <div className="game-container">
            {testKind === GAME_KINDS.DUAL_N_BACK && <NewDualNBack saveTemplate={saveTemplate(current)}
                                                                  changeCurrent={changeCurrent}
                                                                  createTest={createTest}
                                                                  current={current}/>}
            {testKind === GAME_KINDS.SHAPE_SHIFTER && <NewShapeShifter saveTemplate={saveTemplate(current)}
                                                                       changeCurrent={changeCurrent}
                                                                       createTest={createTest}
                                                                       current={current}/>}
            {testKind === GAME_KINDS.COLLISIONS && <NewCollisionDetection saveTemplate={saveTemplate(current)}
                                                                    changeCurrent={changeCurrent}
                                                                    createTest={createTest}
                                                                    current={current}/>}
            }
        </div>
    )
};

export default Game;
