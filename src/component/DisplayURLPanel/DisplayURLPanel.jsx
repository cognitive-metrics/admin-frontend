import React, { useEffect, useState } from "react";
import authFetch from "../../globals/authFetch";
import "./display-url-panel.css"

const DisplayURLPanel = ({url, enableNewTest}) => {

  const [displayIsCopied, setDisplayIsCopied] = useState(false);

  useEffect(() => {
    setTimeout(() => setDisplayIsCopied(false), 2000);
  }, [displayIsCopied, setDisplayIsCopied]);

  return (
    <div className="url-display-panel-container">
      <div className="url-display-panel">
        <div className="url-display-panel-title">
          Test URL:
        </div>
        <div className="url-display-box">
          {url}
        </div>
        {displayIsCopied && <div className="copied-to-clipboard-message-container">
          URL copied to clipboard
        </div>}
        <div className="url-display-action-buttons-container">
          <button className="url-display-action-button"
                  onClick={() => navigator.clipboard.writeText(url)
                    .then(() => setDisplayIsCopied(true))}>
            COPY
          </button>
          <button className="url-display-action-button"
                  onClick={enableNewTest}>
            REPEAT
          </button>
          <button className="url-display-action-button"
                  onClick={() => {
                    const urlArray = url.split("/");
                    authFetch("http://localhost:8080/api/dual-n-back/" + urlArray[urlArray.length - 1],
                      {
                        method: "DELETE"
                      }
                    ).then(enableNewTest)
                  }
                  }>
            DELETE
          </button>
        </div>
      </div>
    </div>
  );
};

export default DisplayURLPanel;
