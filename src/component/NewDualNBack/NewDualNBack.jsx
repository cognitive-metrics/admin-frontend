import React from "react";
import "./new-dual-n-back.css";

const NewDualNBack = ({saveTemplate, changeCurrent, createTest, current}) => (
  <div className="new-test-instance">
    <div className="test-properties-entry-form">
      <div className="property-input-container">
        Number of tests in round:
        <input className="property-input"
               type="number"
               value={current.noOfTests}
               min="1"
               max="100"
               onChange={event => changeCurrent({noOfTests: parseInt(event.target.value, 10)})}/>
      </div>
      <div className="property-input-container">
        Number of correct chars:
        <input className="property-input"
               type="number"
               value={current.noOfVerbalHits}
               min="0"
               max={current.noOfTests}
               onChange={event => changeCurrent({noOfVerbalHits: parseInt(event.target.value, 10)})}/>
      </div>
      <div className="property-input-container">
        Number of correct spatial configurations:
        <input className="property-input"
               type="number"
               value={current.noOfSpatialHits}
               min="0"
               max={current.noOfTests}
               onChange={event => changeCurrent({noOfSpatialHits: parseInt(event.target.value, 10)})}/>
      </div>
      <div className="property-input-container">
        Time per test (ms):
        <input className="property-input"
               type="number"
               value={current.timePerTest}
               min="0"
               onChange={event => changeCurrent({timePerTest: parseInt(event.target.value, 10)})}/>
      </div>
      <div className="property-input-container">
        N-Back level:
        <input className="property-input"
               type="number"
               value={current.level}
               min="1"
               onChange={event => changeCurrent({level: parseInt(event.target.value, 10)})}/>
      </div>
      <div className="property-input-container">
        Subject ID:
        <input className="property-input"
               type="text"
               value={current.subjectId}
               onChange={event => changeCurrent({subjectId: event.target.value})}/>
      </div>
      <div className="property-input-container">
        Number of rounds:
        <input className="property-input"
               type="number"
               min="1"
               value={current.noOfRounds}
               onChange={event => changeCurrent({noOfRounds: parseInt(event.target.value, 10)})}/>
      </div>
    </div>
    <div className="test-properties-actions">
      <button className="test-action-button"
              onClick={saveTemplate}>Save template
      </button>
      <button className="test-action-button"
              onClick={createTest}>
        Generate test
      </button>
    </div>
  </div>
);

export default NewDualNBack;
