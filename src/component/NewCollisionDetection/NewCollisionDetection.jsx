import React from "react";

const NewCollisionDetection = ({saveTemplate, changeCurrent, createTest, current}) => (
    <div className="new-test-instance">
        <div className="test-properties-entry-form">
            <div className="property-input-container">
                Seed:
                <input className="property-input"
                       type="text"
                       value={current.seed}
                       onChange={event => changeCurrent({seed: event.target.value})}/>
            </div>
            <div className="property-input-container">
                Test length (ms):
                <input className="property-input"
                       type="number"
                       value={current.maxGameElapsed}
                       min="1000"
                       onChange={event => changeCurrent({maxGameElapsed: parseInt(event.target.value, 10)})}/>
            </div>
            <div className="property-input-container">
                Minimum point distance:
                <input className="property-input"
                       type="number"
                       value={current.minPointDistance}
                       min="1"
                       onChange={event => changeCurrent({minPointDistance: parseInt(event.target.value, 10)})}/>
            </div>
            <div className="property-input-container">
                Minimum edge distance:
                <input className="property-input"
                       type="number"
                       value={current.minEdgeDistance}
                       min="0"
                       onChange={event => changeCurrent({minEdgeDistance: parseInt(event.target.value, 10)})}/>
            </div>
            <div className="property-input-container">
                Minimum time between collisions (ms):
                <input className="property-input"
                       type="number"
                       value={current.minTimeBetweenCollisions}
                       min="5"
                       max={current.maxTimeBetweenCollisions}
                       onChange={event => changeCurrent({minTimeBetweenCollisions: parseInt(event.target.value, 10)})}/>
            </div>
            <div className="property-input-container">
                Maximum time between collisions (ms):
                <input className="property-input"
                       type="number"
                       value={current.maxTimeBetweenCollisions}
                       min={current.minTimeBetweenCollisions}
                       onChange={event => changeCurrent({maxTimeBetweenCollisions: parseInt(event.target.value, 10)})}/>
            </div>
            <div className="property-input-container">
                Minimum time visible before collision:
                <input className="property-input"
                       type="number"
                       value={current.minTimeVisibleBeforeCollision}
                       min="1"
                       onChange={event => changeCurrent({minTimeVisibleBeforeCollision: parseInt(event.target.value, 10)})}/>
            </div>
            <div className="property-input-container">
                Maximum time visible before collision:
                <input className="property-input"
                       type="number"
                       min="1"
                       value={current.maxTimeVisibleBeforeCollision}
                       onChange={event => changeCurrent({maxTimeVisibleBeforeCollision: parseInt(event.target.value, 10)})}/>
            </div>
            <div className="property-input-container">
                Disappear after:
                <input className="property-input"
                       type="number"
                       min="1"
                       value={current.disappearAfter}
                       onChange={event => changeCurrent({disappearAfter: parseInt(event.target.value, 10)})}/>
            </div>
            <div className="property-input-container">
                Refresh rate (ms/frame):
                <input className="property-input"
                       type="number"
                       min="10"
                       value={current.rerenderRate}
                       onChange={event => changeCurrent({rerenderRate: parseInt(event.target.value, 10)})}/>
            </div>
            <div className="property-input-container">
                Point radius:
                <input className="property-input"
                       type="number"
                       min="1"
                       value={current.pointRadius}
                       onChange={event => changeCurrent({pointRadius: parseInt(event.target.value, 10)})}/>
            </div>
            <div className="property-input-container">
                Subject ID:
                <input className="property-input"
                       type="text"
                       value={current.subjectId}
                       onChange={event => changeCurrent({subjectId: event.target.value})}/>
            </div>
        </div>
        <div className="test-properties-actions">
            <button className="test-action-button"
                    onClick={saveTemplate}>Save template
            </button>
            <button className="test-action-button"
                    onClick={createTest}>
                Generate test
            </button>
        </div>
    </div>
);

export default NewCollisionDetection;