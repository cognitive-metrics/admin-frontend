import React, { createContext, useContext, useRef, useState } from "react";
import "./modal.css";

const ModalContext = createContext(undefined);

const Modal = () => {

  const {modalContent, setModalContent} = useContext(ModalContext);
  const modalContentRef = useRef(undefined);

  const isClickInside = (event) => modalContentRef.current.contains(event.target);

  return (
    <div className={!!modalContent ? 'modal-container' : 'hidden'}
         onClick={(event) => !isClickInside(event) && setModalContent(undefined)}>
      <div className="modal-content"
           ref={modalContentRef}>
        {modalContent}
      </div>
    </div>
  );
};

const ModalProvider = ({children}) => {

  const [modalContent, setModalContent] = useState(undefined);

  return (
    <ModalContext.Provider value={{modalContent, setModalContent}}>
      <Modal/>
      {children}
    </ModalContext.Provider>
  )

};

export { ModalContext };
export default ModalProvider;
