import React, { useEffect, useState } from "react";
import "./save-template-modal.css";

const SaveTemplateModal = ({save}) => {

  const [isNetworkErr, setIsNetworkErr] = useState(false);
  const [name, setName] = useState('');

  useEffect(() => {
    if (isNetworkErr) {
      setTimeout(() => setIsNetworkErr(false), 2000)
    }
  }, [isNetworkErr]);

  const isEnabled = () => !!name && name.length > 0;

  return (
    <div className="save-template-modal-container">
      <div className="save-template-modal-title">
        New template name:
      </div>
      <input className="save-template-modal-input"
             type="text"
             maxLength={20}
             value={name}
             onChange={event => setName(event.target.value)}/>
      <button className={"save-template-modal-save-button" + (isEnabled() ? "" : " disabled")}
              onClick={() => isEnabled() && save(name).catch(() => setIsNetworkErr(true))}>
        Save
      </button>
      {isNetworkErr && <div className="save-template-modal-network-err">
        Error saving template
      </div>}
    </div>
  );
};

export default SaveTemplateModal;
