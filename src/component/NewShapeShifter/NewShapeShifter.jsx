import React from "react";
import "./new-shape-shifter.css";

const defaultShapeShifter = {
  nameOfTemplate: "default",
  noOfTests: 20,
  minNoOfTransitions: 3,
  maxNoOfTransitions: 5,
  timePerTest: 3000,
  velocity: 1,
  subjectId: '',
  noOfRounds: 1
};

const defaultTemplates = [
  defaultShapeShifter,
  {...defaultShapeShifter, nameOfTemplate: "a", noOfTests: 0},
  {...defaultShapeShifter, nameOfTemplate: "b", noOfTests: 1},
  {...defaultShapeShifter, nameOfTemplate: "c", noOfTests: 2},
  {...defaultShapeShifter, nameOfTemplate: "d", noOfTests: 3}
];

const NewShapeShifter = ({saveTemplate, changeCurrent, createTest, current}) => (
  <div className="new-test-instance">
    <div className="test-properties-entry-form">
      <div className="property-input-container">
        Number of tests in round:
        <input className="property-input"
               type="number"
               value={current.noOfTests}
               min="1"
               max="100"
               onChange={event => changeCurrent({noOfTests: event.target.value})}/>
      </div>
      <div className="property-input-container">
        Minimum number of transitions:
        <input className="property-input"
               type="number"
               value={current.minNoOfTransitions}
               min="0"
               max={current.maxNoOfTransitions}
               onChange={event => changeCurrent({minNoOfTransitions: parseInt(event.target.value, 10)})}/>
      </div>
      <div className="property-input-container">
        Maximum number of transitions:
        <input className="property-input"
               type="number"
               value={current.maxNoOfTransitions}
               min={current.minNoOfTransitions}
               onChange={event => changeCurrent({maxNoOfTransitions: parseInt(event.target.value, 10)})}/>
      </div>
      <div className="property-input-container">
        Time per test (ms):
        <input className="property-input"
               type="number"
               value={current.timePerTest}
               min="0"
               onChange={event => changeCurrent({timePerTest: parseInt(event.target.value, 10)})}/>
      </div>
      <div className="property-input-container">
        Element velocity:
        <input className="property-input"
               type="number"
               value={current.velocity}
               min="1"
               onChange={event => changeCurrent({velocity: parseInt(event.target.value, 10)})}/>
      </div>
      <div className="property-input-container">
        Subject ID:
        <input className="property-input"
               type="text"
               value={current.subjectId}
               onChange={event => changeCurrent({subjectId: event.target.value})}/>
      </div>
      <div className="property-input-container">
        Number of rounds:
        <input className="property-input"
               type="number"
               min="1"
               value={current.noOfRounds}
               onChange={event => changeCurrent({noOfRounds: parseInt(event.target.value, 10)})}/>
      </div>
      <div className="test-properties-actions">
        <button className="test-action-button"
                onClick={saveTemplate}>
          Save template
        </button>
        <button className="test-action-button"
                onClick={createTest}>
          Generate test
        </button>
      </div>
    </div>
  </div>
);

export default NewShapeShifter;
