import React from "react";
import "./header.css";
import { withRouter } from "react-router";

const Header = ({isLoggedIn, history}) => (
  <div className="header-container">
    <div className="header-title-container">
      Cognitive Metrics
    </div>
    {isLoggedIn && <button className="logout-button"
                           onClick={() => {
                             localStorage.removeItem("login");
                             history.replace("/login");
                           }}>
      Log out
    </button>}
  </div>
);

export default withRouter(Header);
