import React, { useCallback, useContext, useEffect } from "react";
import { TemplateContext } from "../../container/context/template/Template";
import "./template-selector.css";

const TemplateSelector = () => {

  const {templates, templateChoice, chooseTemplate, deleteTemplate} = useContext(TemplateContext);

  const chosenTemplateIndex = useCallback(() => templateChoice.isChosen ? templateChoice.index : undefined,
    [templateChoice]);

  useEffect(() => {
    console.log("TEMPLATE SELECTOR");
    console.log("State changed", {templates, templateChoice});
    console.log("Chosen template: " + chosenTemplateIndex());
  }, [templates, templateChoice, chosenTemplateIndex]);

  const isChosen = (index) => chosenTemplateIndex() === index;

  return (
    <div className="test-template-chooser">
      <div className="templates-title">
        Templates
      </div>
      <div className="templates-list-divider"/>
      {templates.map((template, index) =>
        <div className={"templates-list-item " + (isChosen(index) ? "highlight-template" : '')}>
          {isChosen(index) && <span className="bullet">&#8226;</span>}
          <span className="template-name"
                onClick={() => chooseTemplate(index)}>
            {template.nameOfTemplate}
          </span>
          <span className="template-delete-button"
                onClick={() => deleteTemplate(template.nameOfTemplate)}>
            X
          </span>
        </div>)}
    </div>
  )
};

export default TemplateSelector;
