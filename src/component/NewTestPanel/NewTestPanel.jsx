import React, {useContext} from "react";
import "./new-test-panel.css";
import TemplateSelector from "../TemplateSelector/TemplateSelector";
import Game from "../../container/hoc/Game";
import {GAME_KINDS, TemplateContext} from "../../container/context/template/Template";

const NewTestPanel = ({setUrl}) => {
    const {testKind, setTestKind} = useContext(TemplateContext);

    return (
        <div className="new-test-panel">
            <div className="new-test-class-chooser">
                <div className={"test-kind" + (testKind === GAME_KINDS.DUAL_N_BACK && " test-kind-selected")}
                     onClick={() => setTestKind(GAME_KINDS.DUAL_N_BACK)}>
                    Dual N-Back
                </div>
                <div className={"test-kind" + (testKind === GAME_KINDS.SHAPE_SHIFTER && " test-kind-selected")}
                     onClick={() => setTestKind(GAME_KINDS.SHAPE_SHIFTER)}>
                    Shape Shifters
                </div>
                <div className={"test-kind" + (testKind === GAME_KINDS.COLLISIONS && " test-kind-selected")}
                     onClick={() => setTestKind(GAME_KINDS.COLLISIONS)}>
                    Collisions Detection
                </div>
            </div>
            <div className="test-details-container">
                <div className="test-generator-container">
                    <Game testKind={testKind}
                          setUrl={setUrl}/>
                </div>
                <div className="template-container">
                    <TemplateSelector/>
                </div>
            </div>
        </div>
    );
};

export default NewTestPanel;
