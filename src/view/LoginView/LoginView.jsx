import React, { useEffect, useState } from "react";
import { withRouter } from "react-router";
import "./login-view.css";
import Header from "../../component/Header/Header";

const emptyCreds = {
  username: "",
  password: ""
};

const LoginView = ({history}) => {

  const [loginCreds, setLoginCreds] = useState({...emptyCreds});
  const [isLoginError, setIsLoginError] = useState(false);

  useEffect(() => {
    if (localStorage.getItem("login")) {
      history.replace("/");
    }
  });

  useEffect(() => {
    isLoginError && setLoginCreds({...emptyCreds});
  }, [isLoginError, setLoginCreds]);

  const handleSubmit = (event) => {
    event.preventDefault();
    setIsLoginError(false);
    const loginUrlEncoded = window.btoa(loginCreds.username + ':' + loginCreds.password);
    fetch("http://localhost:8080/api/login", {
      headers: {
        Authorization: `Basic ${loginUrlEncoded}`
      }
    }).then(res => {
      if (res.ok) {
        localStorage.setItem("login", loginUrlEncoded);
        history.replace("/");
      } else {
        setIsLoginError(true);
      }
    }).catch(() => setIsLoginError(true))
  };

  return (
    <div className="login-view-container">
      <Header isLoggedIn={false}/>
      <div className="login-view">
        <form className="login-form"
              onSubmit={handleSubmit}>
          <div className="username-input-container">
            <input type="email"
                   value={loginCreds.username}
                   onChange={(event => setLoginCreds({
                     ...loginCreds,
                     username: event.target.value
                   }))}
                   placeholder="email@email.com"
                   className="username-input"/>
          </div>
          <div className="password-input-container">
            <input type="password"
                   value={loginCreds.password}
                   onChange={(event => setLoginCreds({
                     ...loginCreds,
                     password: event.target.value
                   }))}
                   className="password-input"/>
          </div>
          <div className="submit-input-container">
            <input type="submit"
                   value="Log in"
                   className={"submit-input" + (isLoginError ? " rb" : "")}/>
            {isLoginError && <div className="login-error">
              Login failed, please try again
            </div>}
          </div>
        </form>
      </div>
    </div>
  )
};

export default withRouter(LoginView);
