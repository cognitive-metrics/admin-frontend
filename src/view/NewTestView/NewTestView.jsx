import React, { useEffect, useState } from "react";
import { withRouter } from "react-router";
import DisplayURLPanel from "../../component/DisplayURLPanel/DisplayURLPanel";
import NewTestPanel from "../../component/NewTestPanel/NewTestPanel";
import Header from "../../component/Header/Header";
import "./new-test-view.css"

const NewTestView = ({history}) => {

  const [testUrl, setTestUrl] = useState();

  useEffect(() => {
    if (!localStorage.getItem("login")) {
      history.replace("/login")
    }
  });

  return (
    <div className="new-test-view-container">
      <Header isLoggedIn={true}/>
      <div className="new-test-view">
        {!!testUrl
          ? <DisplayURLPanel url={testUrl}
                             enableNewTest={() => setTestUrl(undefined)}/>
          : <NewTestPanel setUrl={setTestUrl}/>
        }
      </div>
    </div>
  )
};


export default withRouter(NewTestView);
